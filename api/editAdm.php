<?
require "../uteis.php";

$adm = new Administradora();
if($adm->editarAdm($_POST)){
    $result = array(
        "status"=> 'success',
        "msg" => "Administradora foi editado"
    );
    echo json_encode($result);
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Não foi possivel editar administradora"
    );
    echo json_encode($result);
}

?>