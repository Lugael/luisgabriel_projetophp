<?
require "../uteis.php";

$unidade = new Unidade();
if($unidade->editarUnid($_POST)){
    $result = array(
        "status"=> 'success',
        "msg" => "Unidade foi editado"
    );
    echo json_encode($result);
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Não foi possivel editar unidade"
    );
    echo json_encode($result);
}

?>