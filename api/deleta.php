<?
require "../uteis.php";

$cliente = new Cliente();
if($cliente->deleletarCliente($_POST['id'])){
    
    $totalRegistros = $cliente->getClientes()['totalResult'];;

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu registro foi deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Seu registro não foi deletado"
    );
    echo json_encode($result);
}
?>