<?
require "../uteis.php";

$conselho = new Conselho();
if($conselho->editarConselho($_POST)){
    $result = array(
        "status"=> 'success',
        "msg" => "Conselho foi editado"
    );
    echo json_encode($result);
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Não foi possivel editar conselho"
    );
    echo json_encode($result);
}

?>