<?
require "../uteis.php";

$users = new User();
if($users->deletarUser($_POST['id'])){
    
    $totalRegistros = $users->listaUser()['totalResult'];;

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Usuario foi deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Usuario não foi deletado"
    );
    echo json_encode($result);
}
?>