<?
require "../uteis.php";

$cliente = new Cliente();
if($cliente->editCliente($_POST)){
    $result = array(
        "status"=> 'success',
        "msg" => "Inquilino foi editado"
    );
    echo json_encode($result);
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Não foi possivel editar seu registro"
    );
    echo json_encode($result);
}

?>