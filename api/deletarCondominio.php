<?
require "../uteis.php";

$condominio = new Condominio();
if($condominio->deletarCon($_POST['id'])){
    
    $totalRegistros = $condominio->listCon()['totalResult'];

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Condominio deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Condominio não foi deletado"
    );
    echo json_encode($result);
}
?>