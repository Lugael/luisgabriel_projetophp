<?
require "../uteis.php";

$unidade = new Unidade();
if($unidade->deletarUnid($_POST['id'])){
    
    $totalRegistros = $unidade->listUnid()['totalResult'];;

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Unidade deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Unidade não foi deletado"
    );
    echo json_encode($result);
}
?>