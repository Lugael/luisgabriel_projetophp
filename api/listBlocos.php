<?
require "../uteis.php";

$bloco = new Bloco();
$dados = $bloco->getBlocoFromCond($_REQUEST['id']);
if(!empty($dados)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );
}else{
    $result= array(
        "status" => 'danger',
        "msg" => "o cadastro não pode ser inserido"
    );
}
echo json_encode($result);
?>