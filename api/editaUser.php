<?
require "../uteis.php";

$users = new User();
$dados = array();
foreach($_POST['g'] as $key=>$value){
    $dados[$key] = $value;
}
if($users->editarUser($dados)){
    $result = array(
        "status"=> 'success',
        "msg" => "Usuario foi editado"
    );
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Não foi possivel editar o Usuario"
    );
}
echo json_encode($result);
?>