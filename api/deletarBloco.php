<?
require "../uteis.php";

$bloco = new Bloco();
$result = $bloco->deletarBlo($_POST['id']);
if($result){
    
    $totalRegistros = $bloco->listBloco()['totalResult'];;

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "bloco deletado"
    );
   
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "bloco não foi deletado"
    );
}
echo json_encode($result);

?>