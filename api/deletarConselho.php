<?
require "../uteis.php";

$conselho = new Conselho();
if($conselho->deletarConselho($_POST['id'])){
    
    $totalRegistros = $conselho->listConselho()['totalResult'];

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Conselho deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Conselho não foi deletado"
    );
    echo json_encode($result);
}
?>