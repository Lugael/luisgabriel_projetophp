<?
require "../uteis.php";

$adm = new Administradora();
if($adm->deletarAdm($_POST['id'])){
    
    $totalRegistros = $adm->listAdm()['totalResult'];

    $result = array(
        "status"=> 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Administradora deletado"
    );
    echo json_encode($result);
    
}else{
    $result = array(
        "status"=> 'danger',
        "msg" => "Administradora não foi deletado"
    );
    echo json_encode($result);
}
?>