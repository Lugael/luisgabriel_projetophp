<?  include "uteis.php";?>


<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <title>ApControl</title>
</head>
<body >
    <main class="container">
        <div class="login">
            <div class="card text-center" id="listaCondEmorad" style="width: 60vh; ">
          
                <div class="card-body">
                    <form action="<?=$url_site?>/controllers/restrito.php" method="POST" class="form-signin">
                        <img class="mb-4" src="img/logo.png" alt="" width="200" height="150">  
                        <h1 class="h3 mb-3 font-weight-bold text-light">Apcontrol</h1>
                        <div class="form-group">
                            <input type="text" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
                            <input type="password" name="senha" class="form-control" placeholder="Senha" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js?v=<?=rand(0,9999)?>"></script>
    <? if(isset($_GET['msg'])){?>
    <script>
        $(function(){
            myAlert('danger','<?=$_GET['msg']?>','main');
        })
    </script>
    <?}?>
</body>
</html>
