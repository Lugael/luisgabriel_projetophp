<?  include "uteis.php";
$user = new Usuario();
if(!$user->acesso()){
    header("Location: login.php");
}
if($_GET['page'] == 'logout'){
    if($user->logout()){
    header('Location: '.$url_site.'login.php');
  }
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
    <link rel="stylesheet" href="<?=$url_site?>icofont/icofont.min.css">
    <title>ApControl</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <img src="<?=$url_site?>img/logo.png" href="<?=$url_site?>dashbord" style="width: 10%;">
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="text-light"> <i class="icofont-navigation-menu"></i></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
          <a class="navbar-nav mt-2 mt-lg-0 text-decoration-none" href="<?=$url_site?>dashboard"><i class="icofont-home"></i></a>
            <div class="dropdown navbar-nav  mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropCond" data-toggle="dropdown" aria-expanded="false">
                Condominio
              </button>
              <div class="dropdown-menu" aria-labelledby="dropCond">
                <a class="dropdown-item" href="<?=$url_site?>condominio">Cadastro Condominio</a>
                <a class="dropdown-item" href="<?=$url_site?>listaCondominio">Lista Condominios</a>
              </div>
            </div>
            <div class="dropdown navbar-nav  mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropBloco" data-toggle="dropdown" aria-expanded="false">
                Bloco
              </button>
              <div class="dropdown-menu" aria-labelledby="dropBloco">
                <a class="dropdown-item" href="<?=$url_site?>bloco">Cadastro Bloco</a>
                <a class="dropdown-item" href="<?=$url_site?>listaBloco">Lista Bloco</a>
              </div>
            </div>
            <div class="dropdown navbar-nav  mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropUnid" data-toggle="dropdown" aria-expanded="false">
                Unidade
              </button>
              <div class="dropdown-menu" aria-labelledby="dropUnid">
                <a class="dropdown-item" href="<?=$url_site?>unidade">Cadastro Unidade</a>
                <a class="dropdown-item" href="<?=$url_site?>listaUnidade">Lista Unidade</a>
              </div>
            </div>
            <div class="dropdown navbar-nav  mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                Inquilino
              </button>
              <div class="dropdown-menu" aria-labelledby="dropInqu">
                <a class="dropdown-item" href="<?=$url_site?>cadastro">Cadastro Clientes</a>
                <a class="dropdown-item" href="<?=$url_site?>listaMoradores">Lista Clientes</a>
              </div>
            </div>
            <div class="dropdown navbar-nav  mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropConse" data-toggle="dropdown" aria-expanded="false">
                Conselho
              </button>
              <div class="dropdown-menu" aria-labelledby="dropConse">
                <a class="dropdown-item" href="<?=$url_site?>conselho">Cadastro Conselho</a>
                <a class="dropdown-item" href="<?=$url_site?>listaConselho">Lista Conselho</a>
              </div>
            </div>
            <div class="dropdown navbar-nav mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropAdm" data-toggle="dropdown" aria-expanded="false">
                Administradora
              </button>
              <div class="dropdown-menu" aria-labelledby="dropAdm">
                <a class="dropdown-item" href="<?=$url_site?>administradora">Cadastro Administradora</a>
                <a class="dropdown-item" href="<?=$url_site?>listaAdm">Lista Administradora</a>
              </div>
            </div>
            <div class="dropdown navbar-nav mt-2 mt-lg-0">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropUser" data-toggle="dropdown" aria-expanded="false">
                Usuarios
              </button>
              <div class="dropdown-menu" aria-labelledby="dropUser">
                <a class="dropdown-item" href="<?=$url_site?>usuario">Cadastro Usuarios</a>
                <a class="dropdown-item" href="<?=$url_site?>listaUser">Lista Usuarios</a>
              </div>
            </div>
            <div class="navbar-nav ml-auto mt-2 mt-lg-0">
              <?
              $nome = explode(' ',$_SESSION['USUARIO']['nomeUser'])
              ?>
              <small class="text-white">Olá <?=$nome[0];?>,<br>Sejá bem vindo(a)!</small>
              <a href="<?=$url_site?>logout" class="text-decoration-none"><i class="icofont-logout"></i></a>
            </div>
          </div>
    </nav>
    <main class="container" style="margin-bottom: 4em;">
        <?
        switch ($_GET['page']) {
            case '':
            case 'dashbord':
                require "controllers/dashboard.php";
                require "views/dashboard.php";
                break;
            default:
                require "controllers/".$_GET['page'].'.php';
                require "views/".$_GET['page'].'.php';
                break;
        };
        ?>
    </main>
    <footer class="bg-dark footer  mt-auto  ">
      <div class="d-inline-block col-5 text-light">
        <span>ApControl</span>
        <small>17.113.561/0001-29</small>
        <span>&copy;todos os direitos reservados.</span> 
      </div>
      <div class="d-inline-block col-6 text-info">
        <span>Suporte</span>
        <a href="#" class="text-success"><i class="icofont-whatsapp"></i>(47)99680-7070</a>
      </div>
    </footer>
    <script>var url_site = '<?=$url_site?>';</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?=rand(0,9999)?>"></script>

</body>
</html>
