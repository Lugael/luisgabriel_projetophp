<form action="" method="post" id="formCliente">            
            <div class="form-group">
                <label for="nome"></label>
                <input type="text" name="nome" placeholder="Nome" class="form-control" value="<?=$popular['nome']?>" required>
                <label for="cpf"></label>
                <input type="text" name="cpf" placeholder="CPF" class="form-control" value="<?=$popular['cpf']?>" required>
                <label for="email"></label>
                <input type="email" name="email" placeholder="E-mail" class="form-control" value="<?=$popular['email']?>" required>
                <label for="telefone"></label>
                <input type="text" name="telefone" placeholder="Telefone" class="form-control" value="<?=$popular['telefone']?>">
                <label for="senha"></label>
                <input type="password" name="senha" placeholder="Senha" class="form-control" value="<?=$popular['senha']?>" required>
                <label for="from_condominio"></label><br>
                <select class="fromCondominio custom-select" name="from_condominio" >
                    <option value="">Selecione o Condominio</option>
                    <?foreach($condominios as $condominio){?>
                        <option value="<?=$condominio['id']?>" <?=($condominio['nomeCond'] == $popular['nomeCond'] ? 'selected': '')?>><?=$condominio['nomeCond']?></option>
                    <?}?>
                </select><br>
                <label for="from_bloco"></label><br>
                <select class="fromBloco custom-select" name="from_bloco" >
                    <option value="">Selecione o Bloco</option> 
                    <?
                    if($_GET['id']){
                        $blocos = $inq->getBlocoFromCond($popular['from_condominio']);
                        foreach($blocos['resultSet'] as $bloco){
                            ?>
                    <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeB']?></option>
                    <?}}?>
                </select><br>
                <label for="from_unidade"></label><br>
                <select class="custom-select fromUnid" name="from_unidade" >
                    <option value="">Selecione a Unidade</option>
                    <?
                    if($_GET['id']){
                        $unidades = $inq->getUnidFromBloco($popular['from_bloco']);
                        foreach($unidades['resultSet'] as $unid){
                    ?>
                    <option value="<?=$unid['id']?>"<?=($unid['id'] == $popular['from_unidade'] ? 'selected' : '')?>><?=$unid['numero']?></option>
                    <?}}?>
                </select><br>
            </div>
            <?if($_GET['id']){?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <?}?>
            <button class="btn btn-dark buttonEnviar"type="submit">Enviar</button>
</form>
