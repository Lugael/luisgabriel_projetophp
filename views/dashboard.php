<div class="row">
  <div class="col-12">
    <div class="card m-3 text-center" id="contador">
      <div class="card-body">
        <div  class="row">
            <div class="col-12 col-sm-12 col-md-4">
              <div class="card text-white infor mb-3"  style="max-width: 18rem;">
                  <div class="card-header"><h3>Administradora</h3></div>
                    <div class="card-body">
                      <h5 class="card-text"><?=$qnt['resultSet']['administradora']?></h5>
                      <span><a href="<?=$Url_site?>listaAdm">Ver Mais</a></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4">
              <div class="card text-white infor mb-3" style="max-width: 18rem;">
                  <div class="card-header"><h3>Condominios</h3></div>
                  <div class="card-body">
                    <h5 class="card-text"><?=$qnt['resultSet']['condominios']?></h5>
                    <span><a href="<?=$Url_site?>listaCondominio">Ver Mais</a></span>
                  </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4">
              <div class="card text-white infor mb-3" style="max-width: 18rem;">
                  <div class="card-header"><h3>Blocos</h3></div>
                  <div class="card-body">
                    <h5 class="card-text"><?=$qnt['resultSet']['blocos']?></h5>
                    <span><a href="<?=$Url_site?>listaBloco">Ver Mais</a></span>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
          <div class="col-12 col-sm-12 col-md-4">
            <div class="card text-white infor mb-3" style="max-width: 18rem;">
                  <div class="card-header"><h3>Unidades</h3></div>
                  <div class="card-body">
                    <h5 class="card-text"><?=$qnt['resultSet']['unidades']?></h5>
                    <span><a href="<?=$Url_site?>listaUnidade">Ver Mais</a></span>
                  </div>
              </div>
            </div>
          <div class="col-12 col-sm-12 col-md-4">
            <div class="card text-white infor mb-3" style="max-width: 18rem;">
                <div class="card-header"><h3>Inquilinos</h3></div>
                <div class="card-body">
                  <h5 class="card-text"><?=$qnt['resultSet']['moradores']?></h5>
                  <span><a href="<?=$Url_site?>listaMoradores">Ver Mais</a></span>
                </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-4">
          <div class="card text-white infor mb-3" style="max-width: 18rem;">
                <div class="card-header"><h3>Funcionarios</h3></div>
                <div class="card-body">
                  <h5 class="card-text"><?=$qnt['resultSet']['conselho']?></h5>
                  <span><a href="<?=$Url_site?>listaConselho">Ver Mais</a></span>
                </div>
            </div>
          </div>

      </div>
      </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-12 ">
    <div class="card m-3" id="listaCondEmorad">
      <div class="card-body ">
        <table class="table m-auto text-center table-responsive-sm" border="1" id="conds" style="height: 50%;">
          <thead class="thead" id="tabledashboard">
            <tr>
              <th scope="col" class="font-weight-bold text-dark"><h3>Condominio</h3></th>
              <th scope="col" class="font-weight-bold text-dark"><h3>Numero de moradores</h3></th>
            </tr>
          </thead>
          <tbody>
            <?
            foreach($result['resultSet'] as $ch =>$value){?>
            <tr>
              <td class="infor"><?=$value['nomeCond']?></td>
              <td class="infor"><?=$value['moradores']?></td>
            <?}?>
            </tr>
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-11 ml-3 mr-3" >
    <h2>Ultimas de administradoras cadastradas</h2>
    <ul class="list-group" id="ultimasAdms">
      <?
      foreach($listaAdm['resultSet'] as $ch =>$value){
      ?>
      <li class="list-group-item infor border-bottom"><?=$value['nomeAdm']?></li>
      <?}?>
    </ul>
  </div>
</div>
