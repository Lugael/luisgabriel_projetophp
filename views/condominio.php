<form action="" method="post" id="formCondominio">            
    <div class="form-group">
        <label for="nomeCond"></label>
        <input type="text" name="nomeCond" placeholder="Nome" class="form-control" value="<?=$popular['nomeCond']?>" required>
        <label for="qtdBloco"></label>
        <input type="text" name="qtdBloco" placeholder="Quantidade de bloco" class="form-control" value="<?=$popular['qtdBloco']?>" required>
        <label for="cep"></label>
        <input type="text" name="cep" placeholder="CEP" class="form-control" value="<?=$popular['cep']?>" required>
        <label for="logradouro"></label>
        <input type="text" name="logradouro" placeholder="Logradouro" class="form-control" value="<?=$popular['logradouro']?>" required>
        <label for="numero"></label>
        <input type="text" name="numero" placeholder="Numero" class="form-control" value="<?=$popular['numero']?>">
        <label for="bairro"></label>
        <input type="text" name="bairro" placeholder="Bairro" class="form-control" value="<?=$popular['bairro']?>" required>
        <label for="cidade"></label>
        <input type="text" name="cidade" placeholder="Cidade" class="form-control" value="<?=$popular['cidade']?>" required>
        <label for="estado"></label><br>
        <select name="estado" class="custom-select">
            <option value="">Selecione o Estado:</option>
            <?foreach($estados as $uf=>$vEstado){?>
                <option value="<?=$uf?>"<?=($uf == $popular['estado'] ? 'selected': '')?>><?=$vEstado?></option>
            <?}?>
        </select><br>
        <label for="nomeCons"></label><br>
        <select name="from_sindico"  class="custom-select">
            <option value="">Selecione o Sindico:</option>
            <?foreach($sindicos as $sindico){?>
                <option value="<?=$sindico['id']?>" <?=($sindico['nomeCons'] == $popular['nomeCons'] ? 'selected': '')?>><?=$sindico['nomeCons']?></option>
            <?}?>
        </select><br>
        <label for="from_adm"></label><br>
        <select name="from_adm"  class="custom-select">
            <option value="">Selecione a Administradora:</option>
            <?foreach($administradoras as $administradora){?>
                <option value="<?=$administradora['id']?>" <?=($administradoras['id'] == $popular['from_condominio'] ? 'selected': '')?>><?=$administradora['nomeAdm']?></option>
            <?}?>
        </select><br>
        <input type="hidden" name="from_adm" value="1">
    </div>
    <?if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button class="btn btn-dark buttonEnviar"type="submit">Enviar</button>
</form>
