<form action="" method="post" id="formBloco">            
    <div class="form-group">
        <label for="nomeB"></label>
        <input type="text" name="nomeB" class="form-control"  placeholder="Nome" value="<?=$popular['nomeB']?>" required>
        <label for="andares"></label>
        <input type="text" name="andares" class="form-control" placeholder="Quantidade de andares" value="<?=$popular['andares']?>" required>
        <label for="qtdUnid"></label>
        <input type="text" name="qtdUnid" class="form-control" placeholder="Quantidade de unidades" value="<?=$popular['qtdUnid']?>" required>
        <label for="from_condominio"></label><br>
        <select name="from_condominio" class="custom-select" >
            <option value="">Selecione o Condominio</option>
            <?foreach($condominios as $condominio){?>
                <option value="<?=$condominio['id']?>" <?=($condominio['nomeCond'] == $popular['nomeCond'] ? 'selected': '')?>><?=$condominio['nomeCond']?></option>
            <?}?>
        </select><br>
    </div>
    
    <?if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button class="btn btn-dark buttonEnviar"type="submit">Enviar</button>
</form>
