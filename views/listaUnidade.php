<div class="container m-3 container mt-3 justify-content-center">
    <table class="table table-responsive-sm table-responsive-md" border="1" id="listaUnidade">
        <thead class="thead-dark">
            <tr>
                <th  scope="col">
                <Button class="btn btn-info " ><a href="<?=$url_site?>unidade" class="text-light font-weight-bold" >Adicionar</a></Button>
                </th>
                <th scope="col" colspan="12">
                    <form class="form-inline my-2 my-lg-0 filtro" method="GET">
                        <input type="hidden" name="page" value="listaUnidade">
                        <input class="form-control mr-sm-2 termo2" type="number" placeholder="Buscar por nome"  name="b[numero]" aria-label="Search">
                        <select name="b[nomeB]" class="custom-select termo1">
                                <option value="">Filtro bloco</option>
                                <?foreach($blocos as $bloco){?>
                                    <option value="<?=$bloco['nomeB']?>"><?=$bloco['nomeB']?></option>
                                    <?}?>
                            </select>
                        <button class="btn btn-info my-2 mr-2 my-sm-0 " type="submit" id="buscar" disabled>Buscar</button>
                        <a id="voltar" href="<?=$url_site?>listaUnidade" class="btn btn-info my-2 my-sm-0 ">Voltar</a>
                    </form>
                </th>
            </tr>
            <tr>
                <th scope="col">Numero</th>
                <th scope="col">Metragem</th>
                <th scope="col">Quantidade de vagas</th>
                <th scope="col">Bloco</th>
                <th scope="col">Condominio</th>
                <th scope="col" colspan="2"></th>
            </tr>
        </thead>
            <?foreach($result['resultSet'] as $dados){?>
                <tr data-id="<?=$dados['id']?>">
                    <td class="bg-light"><?= $dados['numero'] ?></td>
                    <td class="bg-light"><?= $dados['metragem'] ?></td>
                    <td class="bg-light"><?= $dados['qtdVagas'] ?></td>
                    <td class="bg-light"><?= $dados['nomeB'] ?></td>
                    <td class="bg-light"><?= $dados['nomeCond'] ?></td>
                    <td class="bg-light"><button id="edit" class="btn btn-dark"><a href="<?=$url_site?>unidade/id/<?=$dados['id']?>"><i class="icofont-ui-edit"></i></button></td>
                    <td class="bg-light"><button id="del" class="btn btn-dark"><a href="#" data-id="<?=$dados['id']?>" class="removerunidade"><i class="icofont-ui-delete"></i></a></button></td>
                </tr>
            <?} ?>
    </table>
    <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-5">
                <div class="bg-dark qtRegistros col-8 text-light ">
                    <h5>Total Registro <span class="badge bg-info totalRegistros"><?=$totalRegistros?></span></h5>
                </div>
            </div>
            <div class="col-3 col-sm-3 col-md-2 ">
                <div class="bg-dark pagina">
                    <?=$paginas?>
                </div>
            </div>
    </div>
</div>
