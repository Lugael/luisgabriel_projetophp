<form action="" method="post" id="formUnidade">            
    <div class="form-group">
        <label for="numero"></label>
        <input type="number" name="numero" class="form-control" placeholder="Numero" value="<?=$popular['numero']?>" required>
        
        <label for="metragem"></label>
        <input type="text" name="metragem" class="form-control" placeholder="Metragem da unidade" value="<?=$popular['metragem']?>" required>
        <label for="qtdVagas"></label>
        <input type="text" name="qtdVagas" class="form-control" placeholder="Quantidade de vagas" value="<?=$popular['qtdVagas']?>" required>
        <label for="from_condominio"></label><br>
                <select class="fromCondominio custom-select" name="from_condominio" >
                    <option value="">Selecione o Condominio</option>
                    <?foreach($condominios as $condominio){?>
                        <option value="<?=$condominio['id']?>" <?=($condominio['nomeCond'] == $popular['nomeCond'] ? 'selected': '')?>><?=$condominio['nomeCond']?></option>
                    <?}?>
                </select><br>
                <label for="from_bloco"></label><br>
                <select class="fromBloco custom-select" name="from_bloco" >
                    <option value="">Selecione o Bloco</option> 
                    <?
                    if($_GET['id']){
                        $blocos = $unid->getBlocoFromCond($popular['from_condominio']);
                        foreach($blocos['resultSet'] as $bloco){
                            ?>
                    <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeB']?></option>
                    <?}}?>
                </select><br>
            </div>
    
    <?if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button class="btn btn-dark buttonEnviar"type="submit">Enviar</button>
</form>
