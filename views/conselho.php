<form action="" method="post" id="formConselho">            
    <div class="form-group">
        
             
        <label for="nomeCons"></label>
        <input type="text" name="nomeCons" placeholder="Nome" class="form-control" value="<?=$popular['nomeCons']?>" required>
        <label for="funcao"></label><br>
        <select name="funcao" class="custom-select">
            <option value="">Selecione a Função</option>
            <?foreach($funcoes as $ch =>$fun){?>
                <option value="<?=$ch?>" <?=($fun == $popular['funcao'] ? 'selected': '')?> ><?=$fun?></option>
            <?}?>
        </select><br>
        <label for="from_condominio"></label><br>
        <select name="from_condominio" class="custom-select" >
            <option value="">Selecione o Condominio </option>
            <?foreach($condominios as $condominio){?>
                <option value="<?=$condominio['id']?>" <?=($condominio['nomeCond'] == $popular['nomeCond'] ? 'selected': '')?>><?=$condominio['nomeCond']?></option>
            <?}?>
        </select><br>
    </div>
    <?if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button class="btn btn-dark buttonEnviar"type="submit">Enviar</button>
</form>
