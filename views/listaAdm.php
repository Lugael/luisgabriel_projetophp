<div class="container m-3 container mt-3 justify-content-center">
    <table class="table table-responsive-sm" border="1" id="listaAdm">
            <thead class="thead-dark">
                <tr>
                    <th  scope="col">
                    <Button class="btn btn-info " ><a href="<?=$url_site?>usuario" class="text-light font-weight-bold" >Adicionar</a></Button>
                    </th>
                    <th scope="col" colspan="12">
                        <form class="form-inline my-2 my-lg-0 filtro" method="GET">
                            <input type="hidden" name="page" value="listaAdm">
                            <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome"  name="b[nomeAdm]" aria-label="Search">
                            <button class="btn btn-info my-2 mr-2 my-sm-0" type="submit" id="buscar" disabled>Buscar</button>
                            <a id="voltar" href="<?=$url_site?>listaAdm" class="btn btn-info my-2 my-sm-0 ">Voltar</a>
                        </form>
                    </th>
                </tr>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">CNPJ</th>
                    <th scope="col" colspan="2"></th>
                </tr>
            </thead>
            <?foreach($result['resultSet'] as $dados){?>
                <tr data-id="<?=$dados['id']?>">
                    <td class="bg-light"><?= $dados['nomeAdm'] ?></td>
                    <td class="bg-light"><?= $dados['cnpj'] ?></td>
                    <td class="bg-light"><button id="edit" class="btn btn-dark"><a href="<?=$url_site?>administradora/id/<?=$dados['id']?>"><i class="icofont-ui-edit"></i></button></td>
                        <td class="bg-light"><button id="del" class="btn btn-dark"><a href="#" data-id="<?=$dados['id']?>" class="removerAdm"><i class="icofont-ui-delete"></i></a></button></td>
                    </tr>
                    <?} ?>
                </table>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-5">
            <div class="bg-dark qtRegistros col-8 text-light ">
                <h5>Total Registro <span class="badge bg-info  totalRegistros"><?=$totalRegistos?></span></h5>
            </div>
        </div>
        <div class="col-4 col-sm-4 col-md-2">
            <div class="bg-dark pagina">
                <?=$paginas?>
            </div>
        </div>
    </div>

</div>
