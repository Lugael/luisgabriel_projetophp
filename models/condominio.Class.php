<?
Class Condominio extends Administradora{  
    protected $id;

    function __construct(){
        
    }
    //listar Condominio
    function listCon($id = null){
        $qry = 'SELECT 
        cond.id,
        cond.nomeCond,
        cond.qtdBloco,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.from_sindico,
        cons.nomeCons,
        adm.nomeAdm,
        cond.from_adm 
        FROM
        ac_condominio cond
        LEFT JOIN ac_conselho cons ON  cond.from_sindico = cons.id
        LEFT JOIN ac_administradora adm ON cond.from_adm = adm.id ';
       $contaTermos = count($this->busca);
       $isNull = false;
       if($contaTermos > 0 && !$isNull){
           $i = 0;
           foreach($this->busca as $field=>$termo){
               if($i == 0 && $termo!=null){
                   $qry = $qry.' WHERE ';
                   $i++;
               }
               switch ($termo){
                   case is_numeric($termo):
                       if(!empty($termo)){
                           $qry = $qry.'cond.'.$field.' = '.$termo.' AND ';
                       }
                       break;

                   default:
                       if(!empty($termo)){
                           $qry = $qry.'cond.'.$field.' LIKE "%'.$termo.'%" AND ';
                       }
                       break;
               }
           }
           $qry = rtrim($qry, ' AND');
       }
       if($id){
           $qry .=' WHERE cond.id ='.$id;
           $unique = true;
       }
       return $this->listarData($qry, $unique);
    }
    //adicionar Condominio
    function adicionarCon($dados){
        $values = '';
        $sql = 'INSERT INTO ac_condominio (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //editar Condominio
    function editarCon($dados){
        $sql = 'UPDATE ac_condominio SET ';

        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);
    }
    //deletar Condominio
    function deletarCon($id){
        $qry = 'DELETE FROM ac_condominio WHERE id='.$id;
        return $this->deletar($qry);

    }
    //Contagem de moradores
    function listaMora(){
        $qry =  'SELECT cond.nomeCond, (SELECT COUNT(inq.from_condominio) FROM ac_inquilino inq WHERE  inq.from_condominio=cond.id) AS moradores
        FROM
        ac_condominio cond';
        $unique = false;
        return $this->listarData($qry, $unique);
    }
}
?>