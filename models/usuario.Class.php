<?
class User extends Dao{
    protected $id;

    function __construct(){

    }
    //lista usuarios
    function listaUser($id = null){
        $qry = 'SELECT
        usuario.id,
        usuario.nomeUser,
        usuario.usuario,
        usuario.senha,
        usuario.dataCadastro
        FROM
        ac_usuarios usuario';
        $contaTermos = count($this->busca);
        $isNull = false;
        if($contaTermos > 0 && !$isNull){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo){
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'usuario.'.$field.' = '.$termo.' AND ';
                        }
                        break;

                    default:
                        if(!empty($termo)){
                            $qry = $qry.'usuario.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE usuario.id ='.$id;
            $unique = true;
        }
    
        return $this->listarData($qry, $unique);
    }
    //adiciona Usuario
    function adcUser($dados){
        $values = '';
        $sql = 'INSERT INTO ac_usuarios (';
        foreach ($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
        if($ch == 'senha'){
            $values .="'".md5($value)."', ";
        }else{
            $values .="'".$value."', ";
        }
        }
        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //edita usuario
    function editarUser($dados){
        $sql = 'UPDATE ac_usuarios SET ';

        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);

    }
    //deletar usuario
    function deletarUser($id){
        $qry = 'DELETE FROM ac_usuarios WHERE id='.$id;
        return $this->deletar($qry);
    }
    //verificando se o usuario existe
    function userExist($users){
        $qry = "SELECT usuario FROM ac_usuarios WHERE usuario = '".$users."'";
        $unique = true;
        return $this->listarData($qry,$unique);
    }


}
?>
