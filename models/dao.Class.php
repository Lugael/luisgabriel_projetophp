<?
class Dao{
    public static $instance;
    public $pagination = 0;
    public $busca = array();

    function __construct(){

    }
    
    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new Dao();
        }
        return self::$instance;
    }
    //Metodo para inserir dados
    public function insertData($qry){
        try{
            $sql =   connectDB::getInstance()->prepare($qry);
            return $sql->execute();
        } catch(Exception $e){
            legivel($e);
            echo 'query: '.$qry;
            return false;
        }
    }
    //Metodo para atualizar dados
    public function updateData($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            if($sql->execute()){
                return true;
            }
            return $sql->execute();
        }catch(Exception $e){
            legivel($e);
            echo 'query: '.$qry;
            return false;
        }
    }
    //Metodo para deletar dados
    public function deletar($qry){
        try{
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            return array(
                'totalResult' => $sql->rowCount()
            );
        }catch(Exception $e){
            legivel($e);
            echo 'query: '.$qry;
            return false;
        }
    }
    //Metodo para listar dados
    public function listarData($qry, $unique=false){
        try{
            if($this->pagination > 0){
                $sql = ConnectDB ::getInstance()->prepare($qry);
                $sql->execute();
                $totalResults = $sql->rowCount();
                $totalPaginas = ceil($totalResults / $this->pagination);
                $qry = $qry.$this->limitPagination($this->pagination);

            }
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            if(!$unique){
                $resultSet = $sql->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $resultSet = $sql->fetch(PDO::FETCH_ASSOC);
            }
            return array(
                'resultSet' => $resultSet,
                'totalResult' => ($totalResults ? $totalResults : $sql->rowCount()),
                'qtPaginas' => ($totalPaginas ? $totalPaginas : 0)
            );
        } catch(Exception $e){
            legivel($e);
            echo 'query: '.$qry;
            return false;
        }
    }
    //Metodo para limitação de páginas
    public function limitPagination($qtRegistros){
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $inicio = ($qtRegistros * $pagAtual - $qtRegistros);

        return " LIMIT " .$inicio. ", ".$qtRegistros;
    }
    //Metodo para rederizar páginas
    public function renderPaginaition($qtPaginas){
        global $url_site;
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $url = $url_site.$_GET['page'].'/'.trataUrl($_GET['b']);
        $estrutura =
        '<nav aria-label="Page navigation example ">
            <ul class="pagination justify-content-center">';
            if($pagAtual > 1){

                $estrutura .= '<li class="page-item "><a class="btn btn-dark" href="'.$url.'pagina/1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
            }
            if($pagAtual > 1){

                for($i=$pagAtual-1; $i < $pagAtual; $i++){

                    $estrutura .= '<li class="page-item"><a class="btn btn-dark" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';

                }
            }
            $estrutura .='<li class="page-item "><a class="btn btn-info border border-secondary" href="'.$url.'pagina/'.$pagAtual.'">'.$pagAtual.'</a></li>';
            if($pagAtual < $qtPaginas){

                for($i=$pagAtual+1; $i < $pagAtual+2; $i++){

                    $estrutura .= '<li class="page-item "><a class="btn btn-dark" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';

                }
            }
            if($pagAtual < $qtPaginas){

                $estrutura .='<li class="page-item"><a class="btn btn-dark" href="'.$url.'pagina/'.$qtPaginas.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
            }
            $estrutura .='</ul>
        </nav>';

        return $estrutura;

    }

}
?>
