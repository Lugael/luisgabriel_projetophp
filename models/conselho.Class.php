<?
class Conselho extends Condominio{
    protected $id;

    function __construct() {

    }
    //listar Conselho
    function listConselho($id = null){
        $qry = 'SELECT
        cons.id,
        cons.nomeCons,
        cons.funcao,
        cons.dataCadastro,
        cond.nomeCond,
        cons.from_Condominio
        FROM
        ac_conselho cons
        LEFT JOIN ac_condominio cond ON cons.from_condominio = cond.id';
        $contaTermos = count($this->busca);
        $isNull = false;
        if($contaTermos > 0 && !$isNull){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo){
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'cons.'.$field.' = '.$termo.' AND ';
                        }
                        break;

                    default:
                        if(!empty($termo)){
                            $qry = $qry.'cons.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE cons.id ='.$id;
            $unique = true;
        }
        $qry .= ' ORDER BY cond.nomeCond, cons.funcao';
        return $this->listarData($qry, $unique);
    }
    //adicionar Conselho
    function adicionarConselho($dados){
        $values = '';
        $sql = 'INSERT INTO ac_conselho (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //editar Conselho
    function editarConselho($dados){
        $sql = 'UPDATE ac_conselho SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);
    }
    //deletar Conselho
    function deletarConselho($id){
        $qry = 'DELETE FROM ac_conselho WHERE id='.$id;
        return $this->deletar($qry);
    }
}
?>
