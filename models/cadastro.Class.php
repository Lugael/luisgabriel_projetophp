<?
Class Cliente extends Unidade{
    protected $id;

    function __construct(){

    }
    //listar Moradores
    function getClientes($id = null){
        $qry = 'SELECT
        inq.id,
        inq.nome,
        inq.cpf,
        inq.email,
        inq.telefone,
        inq.senha,
        inq.datacadastro,
        unid.numero,
        blo.nomeB,
        cond.nomeCond,
        inq.from_unidade,
        inq.from_condominio,
        inq.from_bloco
        FROM
        ac_inquilino inq
        LEFT JOIN ac_unidade unid ON inq.from_unidade = unid.id
        LEFT JOIN ac_bloco blo ON inq.from_bloco = blo.id
        LEFT JOIN ac_condominio cond ON inq.from_condominio = cond.id ';
        $contaTermos = count($this->busca);
        $isNull = false;

        if($contaTermos > 0 && !$isNull){

            $i = 0;

            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo){
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'inq.'.$field.' = '.$termo.' AND ';
                        }
                        break;

                    default:
                        if(!empty($termo)){
                            $qry = $qry.'inq.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE inq.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }
    //adicionar Morador
    function setCliente($dados){
        $values = '';
        $sql = 'INSERT INTO ac_inquilino (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //editar Morador
    function editCliente($dados){
        $sql = 'UPDATE ac_inquilino SET ';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);
    }
    //deletar Morador
    function deleletarCliente($id){
        $qry = 'DELETE FROM ac_inquilino WHERE id='.$id;
        return $this->deletar($qry);

    }

}
?>
