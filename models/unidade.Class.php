<?
Class Unidade extends Bloco{

    protected $id;

    function __construct() {

    }
    //Listar unidades
    function listUnid($id = null){
        $qry = 'SELECT
        unid.id,
        unid.numero,
        unid.metragem,
        unid.qtdVagas,
        unid.dataCadastro,
        cond.nomeCond,
        blo.nomeB,
        unid.from_bloco,
        unid.from_condominio
        FROM
        ac_unidade unid
        LEFT JOIN ac_condominio cond ON unid.from_condominio = cond.id
        LEFT JOIN ac_bloco blo ON unid.from_bloco = blo.id';
        $contaTermos = count($this->busca);
        $isNull = false;
        if($contaTermos > 0 && !$isNull){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo){
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'unid.'.$field.' = '.$termo.' AND ';
                        }
                        break;

                    default:
                        if(!empty($termo)){
                            $qry = $qry.'blo.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE unid.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function getUnidFromBloco($blo){
        $qry = 'SELECT id, numero FROM ac_unidade WHERE from_bloco = '.$blo;
        return $this->listarData($qry);
    }

    function adicionarUnid($dados){
        $values = '';
        $sql = 'INSERT INTO ac_unidade (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    function editarUnid($dados){
        $sql = 'UPDATE ac_unidade SET ';

        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);

    }
    function deletarUnid($id){
        $qry = 'DELETE FROM ac_unidade WHERE id='.$id;
        return $this->deletar($qry);
    }
}
?>
