<?
Class Administradora extends Dao{
    protected $id;

    function __construct(){

    }
    //listar ADMs
    function listAdm($id = null){
        $qry = 'SELECT * FROM ac_administradora adm';
        $contaTermos = count($this->busca);
        $isNull = false;
        if($contaTermos > 0 && !$isNull){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                if(!empty($termo)){
                            $qry = $qry.'adm.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }
    //adicionar ADM
    function adicionarAdm($dados){
        $values = '';
        $sql = 'INSERT INTO ac_administradora (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //editar ADM
    function editarAdm($dados){
        $sql = 'UPDATE ac_administradora SET ';

        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);
    }
    //deletar ADM
    function deletarAdm($id){
        $qry = 'DELETE FROM ac_administradora WHERE id='.$id;
        return $this->deletar($qry);

    }
    //verificação das ultimas ADMs
    function recentesAdM(){
        $qry = 'SELECT nomeAdm, dataCadastro FROM ac_administradora ORDER BY dataCadastro DESC LIMIT 5';

        return $this->listarData($qry);
    }
}
?>
