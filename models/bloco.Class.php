<?
Class Bloco extends Condominio{
    protected $id;

    function __construct() {

    }
    //listar Blocos
    function listBloco($id = null){
        $qry = 'SELECT
        blo.id,
        blo.nomeB,
        blo.andares,
        blo.qtdUnid,
        blo.dataCadastro,
        cond.nomeCond,
        blo.from_condominio
        FROM
        ac_bloco blo
        LEFT JOIN ac_condominio cond ON blo.from_condominio = cond.id';
        $contaTermos = count($this->busca);
        $isNull = false;

        if($contaTermos > 0 && !$isNull){

            $i = 0;

            foreach($this->busca as $field=>$termo){
                if($i == 0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo){
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'blo.'.$field.' = '.$termo.' AND ';
                        }
                        break;

                    default:
                        if(!empty($termo)){
                            $qry = $qry.'blo.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .=' WHERE blo.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }
    //Verificando blocos por condominios
    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeB FROM ac_bloco WHERE from_condominio = '.$cond;
        return $this->listarData($qry);

    }
    //adicionar Bloco
    function adicionarBlo($dados){
        $values = '';
        $sql = 'INSERT INTO ac_bloco (';
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`,';
            $values .="'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }
    //editar Bloco
    function editarBlo($dados){
        $sql = 'UPDATE ac_bloco SET ';

        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE ID='.$dados['editar'];
        return $this->updateData($sql);
    }
    //deletar Bloco
    function deletarBlo($id){
        $qry = 'DELETE FROM ac_bloco WHERE id='.$id;
        return $this->deletar($qry);
    }
}
?>
