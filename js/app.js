$(function(){
    //Administradora
    $('#formadm').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editAdm.php';
            urlRedir = url_site+'listaAdm';
        }else{
            url = url_site+'api/cadastraAdm.php';
            urlRedir = url_site+'administradora';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else {
                    myAlert(data.status ,data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });
    $('#listaAdm').on('click','.removerAdm',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletarAdm.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaAdm');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });
    // Condominio
    $('#formCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editCondominio.php';
            urlRedir = url_site+'Condominio';
        }else{
            url = url_site+'api/cadastrarCondominio.php';
            urlRedir = url_site+'listacondominio';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'listaCondominio');
                }else {
                    myAlert(data.status ,data.msg, 'main');
                }
            
        }
        });
        return false;
    });
    $('#listaCondominio').on('click','.removerCondominio',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletarCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaCondominio');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });
    //bloco
    $('#formBloco').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editBloco.php';
            urlRedir = url_site+'listaBloco';
        }else{
            url = url_site+'api/cadastrarBloco.php';
            urlRedir = url_site+'listaBloco';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else {
                    myAlert(data.status ,data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });
    $('#listaBloco').on('click','.removerbloco',function(){
        var idRegistro = $(this).attr('data-id');
        var urlRedir = url_site+'listaBloco';
        $.ajax({
            url: url_site+'api/deletarBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',urlRedir);
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    //unidade
    $('#formUnidade').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editUnid.php';
            urlRedir = url_site+'listaUnidade';
        }else{
            url = url_site+'api/cadastrarUnidade.php';
            urlRedir = url_site+'unidade';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else {
                    myAlert(data.status ,data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });
    $('#listaUnidade').on('click','.removerunidade',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletarUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaUnidade');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });
    //inquilino
    $('#formCliente').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editCliente.php';
            urlRedir = url_site+'listaMoradores';
        }else{
            url = url_site+'api/cadastraCliente.php';
            urlRedir = url_site+'cadastro';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else {
                    myAlert(data.status ,data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });
    $('#listaClientes').on('click','.removerCliente',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deleta.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaMoradores');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });
    //conselho
    $('#formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editConselho.php';
            urlRedir = url_site+'listaConselho';
        }else{
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'conselho';
        }
        $('.buttonEnviar').attr('disabled',true);
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data : $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else {
                    myAlert(data.status ,data.msg, 'main', urlRedir);
                }
            }
        });
        return false;
    });
    $('#listaConselho').on('click','.removerConselho',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletarConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaConselho');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });
    //Cadastro e edição de Usuarios
    $('#formUser').submit(function(){
        var editar = $(this).find('input[name="g[editar]"]').val();
        var url;
        var urlRedir;
        var senha = $(this).find('input[name="g[senha]"]').val();
        var confirmeSenha = $(this).find('#confSenha').val();
        if(editar){
            url = url_site+'api/editaUser.php';
            urlRedir = url_site+'listaUser';
        }else{
                url = url_site+'api/cadastraUser.php';
                urlRedir = url_site+'usuario';
            }
        $('.buttonEnviar').attr('disabled',true);
        if( senha == confirmeSenha){
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'POST',
                    data : $(this).serialize(),
                    success : function(data){
                        if(data.status == 'success'){
                            myAlert(data.status, data.msg, 'main', urlRedir);
                        }else {
                            myAlert(data.status ,data.msg, 'main', urlRedir);
                        }
                    }
                });
        }else{
            myAlert('danger','Senhas não conferem','main');
        };
        return false;
    });
    //remoção do usuario
    $('#listaUser').on('click','.removerUser',function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletarUser.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main',url_site+'listaUser');
                }else{
                    myAlert(data.status,data.msg,'main');
                }
            }
        });
        return false;
    });


    //select condominio para bloco
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url:url_site+'api/listBlocos.php',
            dataType:'json',
            type: 'POST',
            data: { id: selecionado},
            success: function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeB');
            }
        })
    })
    //select bloco para unidade
    $('.fromBloco').change(function(){
        selecionado = $(this).val();
        $.ajax({
            url:url_site+'api/listUnid.php',
            dataType:'json',
            type:'POST',
            data: { id: selecionado},
            success: function(data){
                selectPopulation('.fromUnid', data.resultSet, 'numero');
            }
        })
    })
    //popular o Select
    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';

        for (let i = 0; i < dados.length; i++) {

            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';

        }
        $(seletor).html(estrutura);
    }
    //envio da busca
    $('.filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/':'';
        termo2 = (termo2) ? termo2+'/':'';

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2

        return false;
    })
    //atualização do button
    $('.termo1, .termo2').on('keyup change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('#buscar').prop('disabled', false);
        }else{
            $('#buscar').prop('disabled', true);
        }
    })


});
function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url = '' : url = url;
        componente = '<div class="alert alert-'+tipo+'role="alert">'+mensagem+'</div>';

        $(pai).prepend(componente);

        setTimeout(function(){
            $(pai).find('div.alert').remove();
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                },500);
            }
        },3000)

    }
