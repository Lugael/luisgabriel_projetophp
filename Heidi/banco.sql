-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para admincondominio
CREATE DATABASE IF NOT EXISTS `admincondominio` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `admincondominio`;

-- Copiando estrutura para tabela admincondominio.ac_administradora
CREATE TABLE IF NOT EXISTS `ac_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(255) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cnpj` varchar(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_administradora: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_administradora` DISABLE KEYS */;
INSERT INTO `ac_administradora` (`id`, `nomeAdm`, `dataCadastro`, `cnpj`) VALUES
	(1, 'Helena e Caroline Ltda', '2022-04-06 08:36:39', '51026358000124'),
	(2, 'Coloradinho', '2022-04-06 08:36:39', '91511270000112'),
	(3, 'Country House', '2022-04-06 13:01:58', '63687957000178'),
	(6, 'Franchesco And WIlosck LTDA', '2022-04-06 13:53:13', '63687957000174'),
	(8, 'Havie LTDA', '2022-04-06 20:40:57', '12415748547'),
	(9, 'Michelangelo entreprise LTDA', '2022-04-06 22:31:02', '45784751478474');
/*!40000 ALTER TABLE `ac_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_bloco
CREATE TABLE IF NOT EXISTS `ac_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeB` varchar(50) NOT NULL,
  `andares` int(11) NOT NULL DEFAULT 0,
  `qtdUnid` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chavCondominio` (`from_condominio`),
  CONSTRAINT `chavCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `ac_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_bloco: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_bloco` DISABLE KEYS */;
INSERT INTO `ac_bloco` (`id`, `nomeB`, `andares`, `qtdUnid`, `dataCadastro`, `from_condominio`) VALUES
	(1, 'violeta', 2, 20, '2022-04-10 21:39:15', 2),
	(2, 'tulipa', 12, 24, '2022-03-30 14:00:22', 2),
	(3, 'orquidia', 12, 24, '2022-03-30 14:00:24', 3),
	(4, 'Lirio', 12, 24, '2022-03-30 14:00:26', 4),
	(5, 'amarilis', 12, 24, '2022-03-30 14:00:27', 5),
	(6, 'hortalicia', 12, 24, '2022-03-30 14:00:28', 6),
	(7, 'portulaca', 12, 24, '2022-03-30 14:00:33', 7),
	(8, 'Grifinória', 5, 20, '2022-03-30 14:00:34', 8),
	(9, 'Sonserina', 5, 20, '2022-03-30 14:00:37', 9),
	(10, 'Corvinal', 3, 15, '2022-03-30 14:00:41', 10),
	(11, 'Lufa-lufa', 5, 20, '2022-03-30 14:00:44', 11),
	(12, 'Starks', 5, 25, '2022-04-11 21:18:07', 12);
/*!40000 ALTER TABLE `ac_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_condominio
CREATE TABLE IF NOT EXISTS `ac_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCond` varchar(255) NOT NULL DEFAULT '',
  `qtdBloco` int(11) NOT NULL DEFAULT 0,
  `cep` varchar(8) NOT NULL DEFAULT '',
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` int(11) NOT NULL DEFAULT 0,
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `estado` varchar(2) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_adm` int(11) DEFAULT NULL,
  `from_sindico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ac_condominio_ac_administradora` (`from_adm`),
  KEY `cha_sindico` (`from_sindico`),
  CONSTRAINT `FK_ac_condominio_ac_administradora` FOREIGN KEY (`from_adm`) REFERENCES `ac_administradora` (`id`),
  CONSTRAINT `cha_sindico` FOREIGN KEY (`from_sindico`) REFERENCES `ac_conselho` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_condominio: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_condominio` DISABLE KEYS */;
INSERT INTO `ac_condominio` (`id`, `nomeCond`, `qtdBloco`, `cep`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `dataCadastro`, `from_adm`, `from_sindico`) VALUES
	(1, 'Bela Vista', 7, '89111090', 'Rua Anfiloquio Nunes Pires', 5471, 'Bela Vista', 'Gaspar', 'SC', '2022-04-01 09:04:54', 1, 1),
	(2, 'Sol Azul', 1, '89032457', 'Rua XV de novembro', 457, 'Centro', 'Blumenau', 'SC', '2022-04-01 09:05:04', 2, 2),
	(3, 'Beira Mar', 8, '89809845', 'Rua Beija-Flor', 541, 'Efapi', 'Chapecó', 'AC', '2022-04-01 10:52:33', 1, 3),
	(4, 'Taparajara', 4, '88161831', 'Rua Teodoro Sampaio', 975, 'Rio Caveiras', 'Biguaçu', 'SC', '2022-04-01 09:05:19', 3, 4),
	(5, 'Teosampaio', 4, '88161028', 'Rua Basilicio Consesso Garcia', 631, 'Universitário', 'Biguaçu', 'SC', '2022-04-01 09:05:26', 3, 5),
	(6, 'Almeida JR', 7, '88104732', 'Rua Tenente Leovegildo Pinheiro', 760, 'Fazenda Santo Antônio', 'São José', 'SC', '2022-04-01 09:05:28', 1, 6),
	(7, 'Velres', 7, '88905544', 'Rodovia SC-447', 586, 'Urussanguinha', 'Araranguá', 'SC', '2022-04-01 09:05:35', 1, 7),
	(8, 'Vitori', 1, '89160058', 'Praça Dias Velho', 905, 'Centro', 'Rio do Sul', 'SC', '2022-04-01 09:05:37', 2, 8),
	(9, 'Backville', 5, '89095450', 'Rua Erich Feldmann', 891, 'Vila Itoupava', 'Blumenau', 'SC', '2022-04-01 09:05:38', 3, 9),
	(10, 'Smalville', 4, '88139290', 'Rua Natalino João Silveira', 466, 'Praia do Meio ', 'Palhoça', 'SC', '2022-04-01 09:05:41', 1, 10),
	(11, 'Borderland', 7, '89806150', 'Rua Sete de Setembro - D', 177, 'Presidente Médici', 'Chapecó', 'SC', '2022-04-01 09:05:42', 3, 11),
	(12, 'ViceCity', 4, '88708651', 'Rua Antônio Rafael Isidoro', 834, 'São Bernardo', 'Tubarão', 'SC', '2022-04-01 09:05:45', 2, 12),
	(41, 'Babalu', 2132, '2121', '2121', 0, 'des', '2121', 'AC', '2022-04-01 10:26:33', 1, 18),
	(42, 'Poze', 2, '2121', '2121', 0, 'des', '2121', 'AC', '2022-04-01 11:51:49', 1, 19);
/*!40000 ALTER TABLE `ac_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_conselho
CREATE TABLE IF NOT EXISTS `ac_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCons` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('Sindico','Subsindico','Conselheiro') NOT NULL DEFAULT 'Sindico',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_Condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chaveCondominio` (`from_Condominio`),
  CONSTRAINT `chaveCondominio` FOREIGN KEY (`from_Condominio`) REFERENCES `ac_condominio` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_conselho: ~61 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_conselho` DISABLE KEYS */;
INSERT INTO `ac_conselho` (`id`, `nomeCons`, `funcao`, `dataCadastro`, `from_Condominio`) VALUES
	(1, 'Mariano', 'Sindico', '2022-04-01 11:18:51', 1),
	(2, 'Julia', 'Sindico', '2022-04-01 09:00:13', 2),
	(3, 'Supi', 'Sindico', '2022-04-01 09:00:14', 3),
	(4, 'Guwin', 'Sindico', '2022-04-01 09:00:16', 4),
	(5, 'Laís Liz da Conceição', 'Sindico', '2022-04-01 09:00:18', 5),
	(6, 'Clarice Emilly Dias', 'Sindico', '2022-04-01 09:00:20', 6),
	(7, 'Marcos Emanuel Yuri Fernandes', 'Sindico', '2022-04-01 09:00:23', 7),
	(8, 'Filipe Carlos Melo', 'Sindico', '2022-04-01 09:00:24', 8),
	(9, 'Lorena Rebeca Pinto', 'Sindico', '2022-04-11 20:59:58', 9),
	(10, 'Leonardo Cauã Pereira', 'Sindico', '2022-04-01 09:00:28', 10),
	(11, 'Alana Esther Almada', 'Sindico', '2022-04-01 09:00:30', 11),
	(12, 'Yuri Erick Filipe Campos', 'Sindico', '2022-04-01 09:00:32', 12),
	(13, 'Lucas', 'Subsindico', '2022-04-01 09:00:35', 1),
	(14, 'Cicero', 'Conselheiro', '2022-04-01 09:00:38', 1),
	(15, 'Hoygu', 'Conselheiro', '2022-04-01 09:00:40', 1),
	(16, 'Waoca', 'Conselheiro', '2022-04-01 09:00:42', 1),
	(17, 'Joao', 'Subsindico', '2022-04-01 09:00:49', 2),
	(18, 'Stefani', 'Conselheiro', '2022-04-01 09:00:53', 2),
	(19, 'Zeywa', 'Conselheiro', '2022-04-01 09:00:55', 2),
	(20, 'Zovaki', 'Conselheiro', '2022-04-01 09:00:57', 2),
	(21, 'Isci', 'Subsindico', '2022-04-01 09:01:04', 3),
	(22, 'Sigirel', 'Conselheiro', '2022-04-01 09:01:06', 3),
	(23, 'Xoigi', 'Conselheiro', '2022-04-01 09:01:08', 3),
	(24, 'Wiudieus', 'Conselheiro', '2022-04-01 09:01:11', 3),
	(25, 'Moude', 'Subsindico', '2022-04-01 09:01:13', 4),
	(26, 'Bierxu', 'Conselheiro', '2022-04-01 09:01:15', 4),
	(27, 'Noyer', 'Conselheiro', '2022-04-01 09:01:17', 4),
	(28, 'Roes', 'Conselheiro', '2022-04-01 09:01:19', 4),
	(29, 'Isis Vanessa Aparecida dos Santos', 'Subsindico', '2022-04-01 09:01:22', 5),
	(30, 'Elisa Amanda Camila da Paz', 'Conselheiro', '2022-04-01 09:01:24', 5),
	(31, 'Raimundo Bernardo Carvalho', 'Conselheiro', '2022-04-01 09:01:26', 5),
	(32, 'Clarice Emilly Dias', 'Conselheiro', '2022-04-01 09:01:29', 5),
	(33, 'Rodrigo Geraldo Renan Figueiredo', 'Subsindico', '2022-04-01 09:01:31', 6),
	(34, 'Luna Carla Carvalho', 'Conselheiro', '2022-04-01 09:01:34', 6),
	(35, 'Andreia Stefany Novaes', 'Conselheiro', '2022-04-01 09:01:36', 6),
	(36, 'Marcelo Thales Cavalcanti', 'Conselheiro', '2022-04-01 09:01:38', 6),
	(37, 'Cristiane Elisa Rosângela da Rocha', 'Subsindico', '2022-04-01 09:01:40', 7),
	(38, 'Luzia Bianca Cardoso', 'Conselheiro', '2022-04-01 09:01:42', 7),
	(39, 'Pietro Pedro Henrique Lorenzo Santos', 'Conselheiro', '2022-04-01 09:01:44', 7),
	(40, 'Vicente Miguel Dias', 'Conselheiro', '2022-04-01 09:01:47', 7),
	(41, 'Sophie Catarina Nogueira', 'Subsindico', '2022-04-01 09:01:49', 8),
	(42, 'Luzia Bianca Cardoso', 'Conselheiro', '2022-04-01 09:01:50', 8),
	(43, 'Isabelly Ester Nascimento', 'Conselheiro', '2022-04-01 09:01:52', 8),
	(45, 'Julio Bryan Henry Peixoto', 'Subsindico', '2022-04-01 09:01:56', 9),
	(46, 'Kevin Breno Moraes', 'Conselheiro', '2022-04-01 09:01:58', 9),
	(47, 'Lorenzo Manoel Almeida', 'Conselheiro', '2022-04-01 09:02:00', 9),
	(48, 'Caroline Isabel Sarah Ramos', 'Conselheiro', '2022-04-01 09:02:02', 9),
	(49, 'Bárbara Sarah Araújo', 'Subsindico', '2022-04-01 09:02:04', 10),
	(50, 'Raimunda Nicole Tereza Porto', 'Conselheiro', '2022-04-01 09:02:06', 10),
	(51, 'Mário Geraldo Gonçalves', 'Conselheiro', '2022-04-01 09:02:08', 10),
	(52, 'Lorenzo Manoel Almeida', 'Conselheiro', '2022-04-01 09:02:10', 10),
	(53, 'Julio Ian Hugo Sales', 'Subsindico', '2022-04-01 09:02:12', 11),
	(54, 'Nicole Bianca Silva', 'Conselheiro', '2022-04-01 09:02:14', 11),
	(55, 'Vitória Nair da Paz', 'Conselheiro', '2022-04-01 09:02:16', 11),
	(56, 'João Bruno Pinto', 'Conselheiro', '2022-04-01 09:02:18', 11),
	(57, 'Marlene Valentina Nina Castro', 'Subsindico', '2022-04-01 09:02:20', 12),
	(58, 'Elias Raimundo Gonçalves', 'Conselheiro', '2022-04-01 09:02:22', 12),
	(59, 'Marcelo Thales Cavalcanti', 'Conselheiro', '2022-04-01 09:02:24', 12),
	(60, 'Raimunda Sophia Mendes', 'Conselheiro', '2022-04-01 09:02:26', 12),
	(64, 'reginal', 'Sindico', '2022-04-10 18:58:02', 10);
/*!40000 ALTER TABLE `ac_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_inquilino
CREATE TABLE IF NOT EXISTS `ac_inquilino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(15) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(50) NOT NULL DEFAULT '0',
  `senha` varchar(50) NOT NULL DEFAULT '0',
  `datacadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_unidade` int(11) DEFAULT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  `from_bloco` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`from_condominio`),
  KEY `chBloco` (`from_bloco`),
  KEY `chUnidade` (`from_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`from_bloco`) REFERENCES `ac_bloco` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `ac_condominio` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `ac_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_inquilino: ~27 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_inquilino` DISABLE KEYS */;
INSERT INTO `ac_inquilino` (`id`, `nome`, `cpf`, `email`, `telefone`, `senha`, `datacadastro`, `from_unidade`, `from_condominio`, `from_bloco`) VALUES
	(1, 'Gustavo Anderson Julio Rocha', '28994542183', 'gustavo_anderson_rocha@mrv.com.br', '47981342641', 'AZowP3EI28', '2022-03-30 14:18:01', 1, 1, 1),
	(2, 'Sebastiana Emilly Beatriz Gomes', '63453008880', 'sebastiana_gomes@chavao.com.b', '47989391347', 'a7yMPF3ngb', '2022-03-30 14:18:06', 1, 1, 1),
	(3, 'Juliana Raimunda Rezende', '88665215743', 'juliana_rezende@mtc.eng.br', '47981669489', 'ngwNJWBCCs', '2022-03-30 14:17:54', 2, 2, 2),
	(4, 'Danilo Mário Julio dos Santos', '78443624728', 'danilomariodossantos@gruposeteestrelas.com.br', '47992514426', '10nik9QYZr', '2022-03-30 14:17:49', 2, 2, 2),
	(5, 'Cristiane Laura Santos', '29153520548', 'cristianelaurasantos@saae.sp.gov.br', '47985849277', 'FOPuiW9dce		', '2022-03-30 14:18:47', 3, 3, 3),
	(6, 'Catarina Kamilly Isabela Jesus', '95040837739', 'catarina_kamilly_jesus@fileno.com.br', '47987258736', '0GKqqwSnXYD', '2022-03-30 14:19:04', 3, 3, 3),
	(7, '	Tiago Ryan Brito', '82656050308', 'tiago_brito@l3ambiental.com.br', '47983985275', 'QEOchlSwQz', '2022-03-30 14:19:38', 4, 4, 4),
	(8, 'Isabelly Bruna Ana Viana', '29053427309', 'isabelly_viana@zaniniengenharia.com.br', '47983744887', 'KpiGzJTyJ1', '2022-03-30 14:20:10', 4, 4, 4),
	(9, 'Carlos Eduardo Enrico Francisco Silva', '54762933961', 'carloseduardosilva@archosolutions.com.br', '47997163148', '9e9Wl62Cri', '2022-03-30 14:21:10', 5, 5, 5),
	(10, 'Rodrigo Ian Drumond', '53450987860', 'rodrigo.ian.drumond@wredenborg.se', '47983353215', 'qkukBTFZwn', '2022-03-30 14:21:35', 5, 5, 5),
	(11, '	Carolina Brenda Andrea Ramos', '51627103422', 'carolina.brenda.ramos@protenisbarra.com.br', '47988273059', 'eCMpGcKPGb', '2022-03-30 14:22:08', 6, 6, 6),
	(12, 'Leandro Thomas Moreira', '	64385897301', 'leandro.thomas.moreira@royalplas.com.br', '	47999089969', 'ZXh5UtitIc', '2022-03-30 14:23:01', 6, 6, 6),
	(13, 'Evelyn Alana Caldeira', '86233386349', 'evelyn.alana.caldeira@technicolor.com', '47988168797', 'NzrHFK6qYT', '2022-03-30 14:23:46', 7, 7, 7),
	(14, 'João Mário Nunes', '36967673793', 'joao_nunes@bol.com.br', '47988426433', '	sRrFROZ8JC', '2022-03-30 14:24:36', 7, 7, 7),
	(15, 'Joana Sabrina Porto', '01103271016', 'joana.sabrina.porto@vivax.com.br', '47985230329', 'zdqlZepkps', '2022-03-30 14:25:33', 8, 8, 8),
	(16, 'Antonio Heitor Henry Bernardes', '54497673901', 'antonio.heitor.bernardes@djapan.com.br', '47993260499', '5hxNlUJFXu', '2022-03-30 14:31:35', 8, 8, 8),
	(17, 'Giovanni Renan Manuel Freitas', '54497673901', 'giovanni-freitas84@vemarbrasil.com.br', '47986116250', 'pMj0DXXlzz', '2022-03-30 14:32:14', 9, 9, 9),
	(18, 'Ian Matheus Gonçalves', '44980865976', 'ian_goncalves@parker.com', '	47987641918', '	2H64uQgOpg', '2022-03-30 14:33:15', 9, 9, 9),
	(19, 'Manuela Lavínia Pereira', '	11584111976', 'manuela.lavinia.pereira@cmfcequipamentos.com.br', '47986341584', 'aq2NqG4yRe', '2022-03-30 14:33:50', 10, 10, 10),
	(20, 'André Otávio Osvaldo Monteiro', '	64153400939', 'andre_monteiro@novotempo.org.br', '47994902444', 'JWjTtXWW1f', '2022-03-30 14:34:31', 10, 10, 10),
	(21, 'César Rafael Oliver Duarte', '13857606991', 'cesar_rafael_duarte@msaengenharia.com.br', '47989067296', 'ql3jehDp63', '2022-03-30 14:34:58', 11, 11, 11),
	(22, 'Luciana Silvana Farias', '62612301968', 'lucianasilvanafarias@accardoso.com.br', '47996612659', 'Wl66TlJ500', '2022-03-30 14:36:06', 11, 11, 11),
	(23, 'Alana Sophie Emilly Barbosa', '74535414920', 'alana_barbosa@phocus.com.br', '47987916960', 'DaOoLs23KZ', '2022-03-30 14:36:36', 12, 12, 12),
	(24, 'Hadassa Brenda Stella Costa', '9247479954', 'hadassa_brenda_costa@a-qualitybrasil.com.br', '47986101788', '7sFNmysKs7', '2022-03-30 14:37:24', 12, 12, 12),
	(28, 'gabriel', '23123', '31231@2121sasasasasas', '31231', '3123', '2022-04-06 22:09:48', 11, 11, 11);
/*!40000 ALTER TABLE `ac_inquilino` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_listaconvidados
CREATE TABLE IF NOT EXISTS `ac_listaconvidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeConv` varchar(255) NOT NULL DEFAULT '0',
  `cpf` varchar(11) NOT NULL,
  `celular` varchar(11) NOT NULL,
  `from_reservar_salao` int(11) NOT NULL DEFAULT 0,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `from_morador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ch_reserva` (`from_reservar_salao`),
  KEY `ch_unid` (`from_unidade`),
  KEY `ch_morador` (`from_morador`),
  CONSTRAINT `ch_morador` FOREIGN KEY (`from_morador`) REFERENCES `ac_inquilino` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ch_reserva` FOREIGN KEY (`from_reservar_salao`) REFERENCES `ac_reserva_salao_festas` (`id`),
  CONSTRAINT `ch_unid` FOREIGN KEY (`from_unidade`) REFERENCES `ac_unidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_listaconvidados: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_listaconvidados` DISABLE KEYS */;
INSERT INTO `ac_listaconvidados` (`id`, `nomeConv`, `cpf`, `celular`, `from_reservar_salao`, `from_unidade`, `from_morador`) VALUES
	(1, 'Emanuel Cauã Mário Bernardes', '05507554110', '62988486602', 1, 1, 7),
	(2, 'Flávia Lorena Emilly Alves', '43723382959', '49983088249', 1, 1, 7),
	(3, 'Vanessa Bruna Letícia Gomes', '73012103961', '48993959939', 1, 1, 7),
	(4, 'Enzo Luís Rodrigo Fernandes', '35006667958', '47985482464', 1, 1, 7),
	(5, 'Thomas Igor Luiz Dias', '18234095986', '48998269638', 1, 1, 7),
	(6, 'Sebastiana Sebastiana Stefany Cardoso', '05592878958', '47985882980', 1, 1, 7),
	(7, 'André Augusto Ramos', '59111440937', '48985988959', 1, 1, 7),
	(8, 'Jaqueline Julia Joana Assunção', '84286596990', '47984038153', 1, 1, 7),
	(9, 'Emilly Julia Cláudia Baptista', '60068358946', '48998645822', 2, 8, 11),
	(10, 'Benício Thales Arthur da Luz', '71413823998', '49993339113', 2, 8, 11),
	(11, 'Lívia Alice Marli Martins', '98941165903', '49986295530', 2, 8, 11),
	(12, 'Leandro Ricardo Alexandre Barros', '35123045910', '47993620031', 2, 8, 11),
	(13, 'Heitor Geraldo Enzo da Mata', '69696474989', '48982353525', 2, 8, 11),
	(14, 'Kaique Renato Gustavo Jesus', '33628813948', '47989603922', 2, 8, 11),
	(15, 'João Ruan Carvalho', '08142789906', '47997630047', 2, 8, 11),
	(16, 'Pietro Leonardo Jesus', '65401658974', '48994518804', 2, 8, 11),
	(17, 'Diego Victor Sales', '81029394946', '47987020523', 3, 4, 1),
	(18, 'Cláudia Mariana Melissa Freitas', '84040215974', '48983112289', 3, 4, 1),
	(19, 'Joaquim Matheus Mário da Costa', '96213585907', '47991468450', 3, 4, 17),
	(20, 'Aline Luna Juliana Gomes', '73879360995', '47992787475', 3, 4, 1),
	(21, 'Raimundo Geraldo Renato Bernardes', '41205534938', '48989058272', 3, 4, 1),
	(22, 'Alice Giovanna Campos', '63005242994', '47996887816', 3, 4, 1),
	(23, 'Daniel Carlos Eduardo Alves', '04123321917', '49992515264', 3, 4, 1),
	(24, 'Hadassa Maitê da Cruz', '90464457980', '48989776232', 3, 4, 1);
/*!40000 ALTER TABLE `ac_listaconvidados` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_pet
CREATE TABLE IF NOT EXISTS `ac_pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(50) DEFAULT NULL,
  `tipo` enum('Cachorro','Gato','Passaro') DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from_unidade` (`from_unidade`),
  CONSTRAINT `chave_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `ac_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_pet: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_pet` DISABLE KEYS */;
INSERT INTO `ac_pet` (`id`, `nomePet`, `tipo`, `dataCadastro`, `from_unidade`) VALUES
	(1, 'careca', 'Gato', '2022-03-30 11:28:33', 3),
	(2, 'Etevaldo', 'Cachorro', '2022-03-30 11:28:46', 3);
/*!40000 ALTER TABLE `ac_pet` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_reserva_salao_festas
CREATE TABLE IF NOT EXISTS `ac_reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tituloEvento` varchar(255) NOT NULL DEFAULT '0',
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` datetime NOT NULL,
  `dataEvento` datetime NOT NULL,
  `from_morador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Chav_unidade` (`from_unidade`),
  KEY `Chav_morador` (`from_morador`),
  CONSTRAINT `Chav_morador` FOREIGN KEY (`from_morador`) REFERENCES `ac_inquilino` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Chav_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `ac_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_reserva_salao_festas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_reserva_salao_festas` DISABLE KEYS */;
INSERT INTO `ac_reserva_salao_festas` (`id`, `tituloEvento`, `from_unidade`, `dataCadastro`, `dataEvento`, `from_morador`) VALUES
	(1, 'Usolxion', 1, '2022-03-30 14:40:27', '2022-05-21 14:00:00', 7),
	(2, 'Aniversário ', 8, '2022-03-30 14:41:59', '2022-04-16 22:00:00', 11),
	(3, 'Churrasco', 4, '2022-03-30 14:42:38', '2022-04-03 11:00:00', 1);
/*!40000 ALTER TABLE `ac_reserva_salao_festas` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_unidade
CREATE TABLE IF NOT EXISTS `ac_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL DEFAULT 0,
  `metragem` float DEFAULT NULL,
  `qtdVagas` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_bloco` int(11) NOT NULL,
  `from_condominio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ac_unidade_ac_condominio` (`from_condominio`),
  KEY `FK_ac_unidade_ac_bloco` (`from_bloco`),
  CONSTRAINT `FK_ac_unidade_ac_bloco` FOREIGN KEY (`from_bloco`) REFERENCES `ac_bloco` (`id`),
  CONSTRAINT `FK_ac_unidade_ac_condominio` FOREIGN KEY (`from_condominio`) REFERENCES `ac_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_unidade: ~13 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_unidade` DISABLE KEYS */;
INSERT INTO `ac_unidade` (`id`, `numero`, `metragem`, `qtdVagas`, `dataCadastro`, `from_bloco`, `from_condominio`) VALUES
	(1, 107, 30, 1, '2022-04-11 20:44:33', 3, 3),
	(2, 102, 25.8, 2, '2022-03-30 14:01:12', 2, 2),
	(3, 103, 25.8, 2, '2022-03-30 14:01:17', 3, 3),
	(4, 208, 30, 1, '2022-03-30 14:01:47', 4, 4),
	(5, 501, 30, 1, '2022-03-30 14:01:49', 5, 5),
	(6, 101, 30, 1, '2022-03-30 14:01:51', 6, 6),
	(7, 304, 30, 1, '2022-03-30 14:01:52', 7, 7),
	(8, 203, 40, 1, '2022-03-30 14:01:55', 8, 8),
	(9, 101, 70, 1, '2022-04-01 11:14:35', 9, 9),
	(10, 303, 50, 1, '2022-03-30 14:01:59', 10, 10),
	(11, 202, 50, 1, '2022-03-30 14:02:01', 11, 11),
	(12, 204, 50, 1, '2022-03-30 14:02:03', 12, 12);
/*!40000 ALTER TABLE `ac_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela admincondominio.ac_usuarios
CREATE TABLE IF NOT EXISTS `ac_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeUser` varchar(255) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela admincondominio.ac_usuarios: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `ac_usuarios` DISABLE KEYS */;
INSERT INTO `ac_usuarios` (`id`, `nomeUser`, `usuario`, `senha`, `dataCadastro`) VALUES
	(1, 'Lui Gabriel Lima Cunha', 'Luis', '81dc9bdb52d04dc20036dbd8313ed055', '2022-04-11 20:47:03'),
	(2, 'Gabrieel', 'lssl', 'caf1a3dfb505ffed0d024130f58c5cfa', '2022-04-06 17:22:01'),
	(3, 'Gabrieelee', 'sasdasa', '81dc9bdb52d04dc20036dbd8313ed055', '2022-04-06 16:52:12'),
	(4, 'Gabrieeleeaa', 'gabriel', '81dc9bdb52d04dc20036dbd8313ed055', '2022-04-06 17:21:43'),
	(8, 'gridnaldo', 'grid', '81dc9bdb52d04dc20036dbd8313ed055', '2022-04-10 16:45:33'),
	(9, 'gridnaldo', 'grid', 'aab3238922bcc25a6f606eb525ffdc56', '2022-04-10 16:50:07'),
	(17, 'luis', 'luisss', 'd9b1d7db4cd6e70935368a1efb10e377', '2022-04-11 21:32:41'),
	(18, 'luis', 'lui', 'd9b1d7db4cd6e70935368a1efb10e377', '2022-04-10 17:56:19');
/*!40000 ALTER TABLE `ac_usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
